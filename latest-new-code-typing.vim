syntax on
:imap jj <Esc>
:set mouse=a
set nocompatible
filetype off


set hlsearch
nnoremap <space> :nohlsearch<CR>
set tabstop=4
set shiftwidth=4
set expandtab
set autoindent

" For swagger
let g:instant_markdown_slow = 1

map <C-n> :NERDTreeToggle<CR>
let g:airline#extension#tabline#enabled = 1
" colorscheme holokai
" colorscheme mango 
" let g:airline_powerline_fonts = 1
" let g:lsc_auto_map = v:true
let g:loaded_clipboard_provider = 1

" let g:coc_disable_startup_warning = 1 
let g:indentLine_faster = 1
nnoremap mm :below vertical terminal<CR>
nnoremap ,, :RunVrc<CR>

" let g:lsc_server_commands = {'dart': 'dart_language_server'}
" let g:dart_format_on_save = 1

" let g:flutter_command 


nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
nnoremap H gT
nnoremap L gt


" nnoremap <Right> <c-w>l  
" nnoremap <Left> <c-w>h  
" nnoremap <Up> <c-w>k 
" nnoremap <Down> <c-w>j 
nmap <F7> :w<cr>:!python %<cr>
nmap <F9> :w<cr>:!flutter run<cr>

" nmap <silent> gd <Plug>(coc-definition)
" nmap <silent> gy <Plug>(coc-type-definition)
" nmap <silent> gi <Plug>(coc-implementation)
" nmap <silent> gr <Plug>(coc-references)


call plug#begin('~/.config/nvim/autoload/plugged')
Plug 'junegunn/seoul256.vim'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'scrooloose/nerdtree'
Plug 'kien/ctrlp.vim'
Plug 'msanders/snipmate.vim'
Plug 'vim-scripts/tComment'
Plug 'dart-lang/dart-vim-plugin'
Plug 'thosakwe/vim-flutter'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'rafi/awesome-vim-colorschemes'
Plug 'flazz/vim-colorschemes'
Plug 'natebosch/vim-lsc'
Plug 'natebosch/vim-lsc-dart'
Plug 'amiralies/coc-elixir', {'do': 'yarn install && yarn prepack'}
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'Yggdroot/indentLine'
Plug 'bewakes/vim-rest-client'
Plug 'suan/vim-instant-markdown'
Plug 'ikatyang/emoji-cheat-sheet'
Plug 'mcchrish/nnn.vim'
Plug 'neovim/nvim-lspconfig'
Plug 'nvim-lua/completion-nvim'
Plug 'hrsh7th/nvim-compe'
Plug 'kabouzeid/nvim-lspinstall'

" Plug 'xavierchow/vim-swagger-preview'
call plug#end()

" Enable Flutter menu

" lua require'nvim_lsp'.tsserver.setup{ on_attach=require'completion'.on_attach }


" Some of these key choices were arbitrary;
" it's just an example.
nnoremap fa :FlutterRun<cr>
nnoremap fq :FlutterQuit<cr>
nnoremap fr :FlutterHotReload<cr>
nnoremap fR :FlutterHotRestart<cr>
nnoremap fD :FlutterVisualDebug<cr>
nnoremap nN :DartFmt<cr>



" if has('vim_starting')
"       set nocompatible
"         set runtimepath+=~/.config/nvim/vim-plug
"     endif
"     filetype plugin indent on

colorscheme buddy 
" hi Normal guibg=NONE ctermbg=NONE
nmap <unique> <leader>e <Plug>GenerateDiagram 



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""FlutterHotReload""""""""""""
