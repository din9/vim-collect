syntax on
:imap jj <Esc>
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')


"for highlight search
set hlsearch
nnoremap <space> :nohlsearch<CR>

" Bagi space yang dkat
set tabstop=4
set shiftwidth=4
set expandtab

" Untuk auto indent
set autoindent

" ctrlp vim
set runtimepath^=~/.vim/bundle/ctrlp.vim
let g:ctrlp_map = '<C-p>'
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']


"   MAP JJ KEYS TO ESCAPE 
:imap jj <Esc>

"   VIM KEYBINDING PANE NAVIGATION
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>



"   TAB NAVIGATION
nnoremap H gT
nnoremap L gt


"   BUFFER TAB KEYMAP
map <leader>t :tabnew<cr>
map <leader>to :tabonly<cr>
map <leader>q :bw<cr>
map <leader>tm :tabmove
map <leader>e :tabedit <c-r>=expand("%:p:h")<cr>/"

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'mattn/emmet-vim'
Plugin 'scrooloose/nerdtree'
Plugin 'rafi/awesome-vim-colorschemes'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'flazz/vim-colorschemes'
Plugin 'kien/ctrlp.vim'
Plugin 'msanders/snipmate.vim'
Plugin 'vim-scripts/tComment'
Plugin 'uguu-org/vim-matrix-screensaver'
Plugin 'lordm/vim-browser-reload-linux'
Plugin 'turbio/bracey.vim'
Plugin 'posva/vim-vue'
Plugin 'dart-lang/dart-vim-plugin'
Plugin 'thosakwe/vim-flutter'


" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" Flutter
nnoremap <leader>fa :FlutterRun<cr>
nnoremap <leader>fq :FlutterQuit<cr>
nnoremap <leader>fr :FlutterHotReload<cr>
nnoremap <leader>fR :FlutterHotRestart<cr>
nnoremap <leader>fD :FlutterVisualDebug<cr>



" CUSTOM MAPPING

map <C-n> :NERDTreeToggle<CR>
colorscheme buddy 

let g:airline#extensions#tabline#enabled = 1

" :cd /mnt/c/laragon/www/contohlaravel
" :cd /mnt/d/react-native/react2021
" :cd /mnt/d/laravel/tuto
" :cd /mnt/d/laravel/multipleInsert
" :cd /mnt/d/vuejs/nuxt
" :cd /mnt/d/react-native/covid
" :cd /mnt/d/reactjs/complete-javascript-course/4-DOM-pig-game
" :cd /mnt/c/laragon/www/commentHero
:cd /mnt/d/flutters/tracker_syam
" :cd /mnt/c/laragon/www/relationship
